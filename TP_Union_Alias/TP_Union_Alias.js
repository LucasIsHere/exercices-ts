// Exercice 1
function returnSomething(arg) {
    var res = 0;
    if (typeof arg === "object") {
        console.log("Je suis la fonction returnSomething,\nme voil\u00E0 en pr\u00E9sence d'un tableau de nombres,\nje vais donc en faire la somme !");
        for (var i = 0; i < arg.length; i++) {
            res = res + arg[i];
        }
        return res;
    }
    else {
        console.log("Un simple nombre ? Tsss");
        return arg;
    }
}
var nombre = 15;
console.log(returnSomething(nombre));
var nombre2 = [15, 20, 75, 510];
console.log(returnSomething(nombre2));
var var1 = 100;
var var2 = 55.248;
var var3 = [1, 2, 3, 4, 5, 6];
var var4 = ["Lucas", "Tristan", "Corentin"];
console.log("var1 nombre entier : ".concat(var1));
console.log("var2 nombre d\u00E9cimal : ".concat(var2));
console.log("var3 tableau de nombres : ".concat(var3));
console.log("var4 tableau de strings : ".concat(var4));
