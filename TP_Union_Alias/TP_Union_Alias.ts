// Exercice 1

function returnSomething(arg: number | number[]): number {
    let res: number = 0;
    if (typeof arg === "object") {
        console.log(`Je suis la fonction returnSomething,\nme voilà en présence d'un tableau de nombres,\nje vais donc en faire la somme !`);
        for (let i = 0; i < arg.length; i++) {
            res = res + arg[i];
        }
        return res;
    }
    else {
        console.log(`Un simple nombre ? Tsss`);
        return arg;
    }
}

let nombre: number = 15;
console.log(returnSomething(nombre));

let nombre2: number[] = [15, 20, 75, 510];
console.log(returnSomething(nombre2));



// Exercice 2

type int = number;
type float = number;
type NumberArray = number[];
type StringArray = string[];

let var1: int = 100;
let var2: float = 55.248;
let var3: NumberArray = [1, 2, 3, 4, 5, 6];
let var4: StringArray = ["Lucas", "Tristan", "Corentin"];

console.log(`var1 nombre entier : ${var1}`);
console.log(`var2 nombre décimal : ${var2}`);
console.log(`var3 tableau de nombres : ${var3}`);
console.log(`var4 tableau de strings : ${var4}`);