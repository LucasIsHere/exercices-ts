var adresse = document.getElementById("adresse");
var adresse2 = document.getElementById("adresse2");
var city = document.getElementById("city");
var btnSave = document.getElementById("btnSave");
var spanVide = document.getElementById("spanVide");
var adresseComplete = document.getElementById("adresseComplete");
// Fonction avec paramètre optionnel pour adresse 2
function compoAdresseOption(arg1, arg3, arg2) {
    return "".concat(arg1, ", ").concat(arg2, ", ").concat(arg3);
}
// Fonction avec paramètre par défaut (N/A) pour adresse 2
function compoAdresseDefault(arg1, arg3, arg2) {
    if (arg2 === void 0) { arg2 = "(N/A)"; }
    return "".concat(arg1, ", ").concat(arg2, ", ").concat(arg3);
}
function effaceInput() {
    adresse.value = "";
    adresse2.value = "";
    city.value = "";
}
btnSave === null || btnSave === void 0 ? void 0 : btnSave.addEventListener("click", function (event) {
    if (adresse2.value === "") {
        adresseComplete.value = compoAdresseDefault(adresse.value, city.value);
        effaceInput();
    }
    else {
        adresseComplete.value = compoAdresseOption(adresse.value, city.value, adresse2.value);
        effaceInput();
    }
});
