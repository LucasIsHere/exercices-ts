let adresse = document.getElementById("adresse") as HTMLInputElement;
let adresse2 = document.getElementById("adresse2") as HTMLInputElement;
let city = document.getElementById("city") as HTMLInputElement;
let btnSave = document.getElementById("btnSave") as HTMLButtonElement;
let spanVide = document.getElementById("spanVide") as HTMLSpanElement;
let adresseComplete = document.getElementById("adresseComplete") as HTMLInputElement;

// Fonction avec paramètre optionnel pour adresse 2
function compoAdresseOption(arg1: string, arg3: string, arg2?: string) {
    return `${arg1}, ${arg2}, ${arg3}`;
}

// Fonction avec paramètre par défaut (N/A) pour adresse 2
function compoAdresseDefault(arg1: string, arg3: string, arg2 = "(N/A)") {
    return `${arg1}, ${arg2}, ${arg3}`;
}

function effaceInput() {
    adresse.value = "";
    adresse2.value = "";
    city.value = "";
}

btnSave?.addEventListener("click", function (event) {
    if (adresse2.value === "") {
        adresseComplete.value = compoAdresseDefault(adresse.value, city.value);
        effaceInput();
    }
    else {
        adresseComplete.value = compoAdresseOption(adresse.value, city.value, adresse2.value);
        effaceInput();
    }
})