var prompt = require('prompt-sync')();

let nombre: number = Number(prompt(`Nombre ? `));

function isArmstrongNumber(arg: number) {
    
    let digits: string[] = arg.toString().split('');
    let res: number = 0;

    for (let i = 0; i < digits.length; i++) {
        res = res + Number(digits[i]) ** digits.length;
    }

    if (nombre === res) {
        console.log(`${nombre} est un nombre Armstrong.`);
    }
    else {
        console.log(`${nombre} n'est pas un nombre Armstrong.`);
    }
}

isArmstrongNumber(nombre);