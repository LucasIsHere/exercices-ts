enum planete {
    mercure = 0.2408467,
    venus = 0.61519726,
    terre = 1,
    mars = 1.8808158,
    jupiter = 11.862615,
    saturne = 29.447498,
    uranus = 84.016846,
    neptune = 164.79132
}

let age: number = 1000000000;
let ageEnAnnees: number = age / 31557600;

function calculAgeSec(age: number) {
    console.log(`
    Age sur Mercure : ${age / planete.mercure}s
    Age sur Vénus : ${age / planete.venus}s
    Age sur Terre : ${age / planete.terre}s
    Age sur Mars : ${age / planete.mars}s
    Age sur Jupiter : ${age / planete.jupiter}s
    Age sur Saturne : ${age / planete.saturne}s
    Age sur Uranus : ${age / planete.uranus}s
    Age sur Neptune : ${age / planete.neptune}s
    `);
}

calculAgeSec(ageEnAnnees);