var planete;
(function (planete) {
    planete[planete["mercure"] = 0.2408467] = "mercure";
    planete[planete["venus"] = 0.61519726] = "venus";
    planete[planete["terre"] = 1] = "terre";
    planete[planete["mars"] = 1.8808158] = "mars";
    planete[planete["jupiter"] = 11.862615] = "jupiter";
    planete[planete["saturne"] = 29.447498] = "saturne";
    planete[planete["uranus"] = 84.016846] = "uranus";
    planete[planete["neptune"] = 164.79132] = "neptune";
})(planete || (planete = {}));
var age = 1000000000;
var ageEnAnnees = age / 31557600;
function calculAgeSec(age) {
    console.log("\n    Age sur Mercure : ".concat(age / planete.mercure, "s\n    Age sur V\u00E9nus : ").concat(age / planete.venus, "s\n    Age sur Terre : ").concat(age / planete.terre, "s\n    Age sur Mars : ").concat(age / planete.mars, "s\n    Age sur Jupiter : ").concat(age / planete.jupiter, "s\n    Age sur Saturne : ").concat(age / planete.saturne, "s\n    Age sur Uranus : ").concat(age / planete.uranus, "s\n    Age sur Neptune : ").concat(age / planete.neptune, "s\n    "));
}
calculAgeSec(ageEnAnnees);
